# spring-project



pipeline { 


   agent any 


 


   stages { 


  stage('CompileandPackage') { 


         steps { 


    git 'https://gitlab.com/rprashipillai10/spring-project.git' 


    sh label: '', script: 'mvn clean package' 


    echo "${BUILD_URL}" 


         } 


      } 


  stage('CodeAnalysisonSonar') { 


         steps { 


            script { 


               def scannerHome = tool 'SonarScanner'; 


                  withSonarQubeEnv("SonarCloud") { 


                     sh "${tool("SonarScanner")}/bin/sonar-scanner" 


                  } 


            } 


        } 


    } 


  stage('DeploytoTomcat') { 


         steps { 


sh label: '', script: 'cp $(pwd)/target/*.war /opt/tomcat/webapps/' 


echo "${BUILD_URL}" 


         } 


      } 


  stage('FunctionalTesting') { 


         steps { 


    sleep 60 


sh label: '', script: 'mvn -Dfilename=testng-functional.xml surefire:test' 


         } 


      } 


   } 


} 

node { 


   def mvnHome 


   def scannerHome 


   stage('Preparation') { 


      git 'https://gitlab.com/rprashipillai10/spring-project.git'            


      mvnHome = tool 'MAVEN_HOME' 


      scannerHome = tool 'SonarScanner' 


   } 


   stage('CompileandPackage') { 


      // Run the maven build 


      withEnv(["MVN_HOME=$mvnHome"]) { 


         if (isUnix()) { 


            sh '"$MVN_HOME/bin/mvn" -Dmaven.test.failure.ignore clean package' 


         } else { 


            bat(/"%MVN_HOME%\bin\mvn" -Dmaven.test.failure.ignore clean package/) 


         } 


      } 


   } 


   stage('CodeAnalysisonSonar') {         


      withSonarQubeEnv("SonarCloud") { 


                     sh "${tool("SonarScanner")}/bin/sonar-scanner" 


                  } 


            } 


   stage('DeploytoTomcat') { 


      sh 'cp $(pwd)/target/*.war /opt/tomcat/webapps/' 


   }  


   stage('FunctionalTesting') { 


      sleep 60 


      sh label: '', script: 'mvn -Dfilename=testng-functional.xml surefire:test' 


   }  


} 
